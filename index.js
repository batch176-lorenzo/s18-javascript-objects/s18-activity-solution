function Pokemon(name, level) {
	this.name = name;
	this.level = level
	this.health = 3 * level;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + ` tackled ` + target.name)
		console.log(target.name + ` 's health is now reduced to ` + (target.health -= this.attack))
		if (target.health <=5 ) {
			target.faint()	
		} 
	} 
	this.faint = function(){ 
		console.log(this.name + ` fainted.`)
	}	
}

let pikachu = new Pokemon(`Pikachu`,40)
let charizard = new Pokemon(`Charizard`, 80)

console.log(pikachu)
console.log(charizard)

// pikachu.tackle(charizard)
// pikachu.tackle(charizard)
// pikachu.tackle(charizard)
// pikachu.tackle(charizard)
// pikachu.tackle(charizard)
// pikachu.tackle(charizard)




